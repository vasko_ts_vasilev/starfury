import { API_KEY } from '../utils/constants';

export const getQueryParamsParams = params => {
    let queryParams = `?api_key=${API_KEY}`;

    for (const [key, value] of Object.entries(params)) {

        if (value) {
            queryParams += `&${key}=${value}`;
        }
    };

    return queryParams;
};