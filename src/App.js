import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'rsuite/dist/rsuite.min.css'; // or 'rsuite/dist/rsuite.min.css'
import { Fragment, useState } from 'react';

import Navigation from '../src/components/Navigation';
import AsteroidsListing from './components/asteroids/AsteroidsListing';
import AsteroidPictureOfDay from './components/apod/AsteroidPictureOfDay';
import { ASTEROIDS, APOD } from './utils/constants';

function App() {
	const initialState = window.location.hash.includes(ASTEROIDS) ? ASTEROIDS : APOD;
	const [route, setRoute] = useState(initialState);

	return (
		<Fragment>
			<Navigation
				route={route}
				setRoute={setRoute} />

			{
				route === ASTEROIDS && <AsteroidsListing />
			}
			{
				route === APOD && <AsteroidPictureOfDay />
			}
		</Fragment>
	);
};


export default App;
