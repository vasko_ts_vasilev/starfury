import React from 'react';
import { Container, Navbar, Nav, NavItem } from 'react-bootstrap';

const Navigation = ({ route, setRoute }) => (
    <Navbar
        bg='dark'
        className=''>
        <Container>
            <Nav
                activeKey={route}
                onSelect={setRoute}>
                <NavItem className='d-flex'>
                    <Nav.Link
                        eventKey='apod' href="#/apod" className='text-white'>APOD</Nav.Link>
                </NavItem>
                <NavItem>
                    <Nav.Link
                        eventKey='asteroids'
                        href='#/asteroids' className='text-white'>Asteroids</Nav.Link>
                </NavItem>
            </Nav>
        </Container>
    </Navbar>
);

export default Navigation;