import React from 'react';

const AsteroidsTableRow = ({
    name,
    diameter,
    approachDate,
    velocity,
    isHazardous
}) => (
    <tr>
        <td>{name}</td>
        <td>{diameter}</td>
        <td>{approachDate}</td>
        <td>{velocity}</td>
        <td>{isHazardous}</td>
    </tr>
);

export default AsteroidsTableRow;