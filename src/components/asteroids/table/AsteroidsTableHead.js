import React from 'react';

const AsteroidsTableHead = () => (
    <thead>
        <tr>
            <th>Name</th>
            <th>Estimated Diameter</th>
            <th>Appr. Hour</th>
            <th>Relative Velocity</th>
            <th>Potentially Hazardous</th>
        </tr>
    </thead>
);

export default AsteroidsTableHead;