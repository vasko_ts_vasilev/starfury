import moment from 'moment';
import React from 'react';
import { Table } from 'react-bootstrap';

import AsteroidsTableHead from './AsteroidsTableHead';
import AsteroidsTableRow from './AsteroidsTableRow';

const AsteroidsTable = ({ entryArr }) => (
    <Table striped bordered hover responsive>
        <AsteroidsTableHead />
        <tbody>
            {
                entryArr[1].map((asteroid, i) => {
                    const { name, close_approach_data, estimated_diameter, is_potentially_hazardous_asteroid } = asteroid;

                    const kmPerHour = +close_approach_data[0].relative_velocity.kilometers_per_hour
                    return <AsteroidsTableRow
                        name={name}
                        diameter={`${estimated_diameter['kilometers'].estimated_diameter_min.toFixed(2)}km - ${estimated_diameter['kilometers'].estimated_diameter_max.toFixed(2)} km`}
                        approachDate={moment(close_approach_data[0]['close_approach_date_full']).format('HH:mm')}
                        velocity={`${kmPerHour.toFixed(0)} km/h`}
                        isHazardous={is_potentially_hazardous_asteroid ? 'yes' : 'no'}
                    />
                })
            }
        </tbody>
    </Table>
);

export default AsteroidsTable;