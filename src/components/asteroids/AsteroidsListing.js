import moment from 'moment';
import React, { useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { DateRangePicker } from 'rsuite';

import { getAsteroidsData } from '../../actions';
import AsteroidsData from './AsteroidsData';

const AsteroidsListing = () => {

    const [data, setData] = useState({});

    const getAsteroids = async (dateRangeArr) => {
        const data = await getAsteroidsData({
            start_date: moment(dateRangeArr[0]).format('YYYY-MM-DD'),
            end_date: moment(dateRangeArr[1]).format('YYYY-MM-DD')
        });

        setData(data);
    }

    const { allowedMaxDays } = DateRangePicker;

    return (
        <Container className=''>
            <h1 className='text-center pt-4'>Asteroids or near Earth objects</h1>
            <Row className='pt-4  justify-content-md-center'>
                <Col sm='12' md='4' lg='4' className='d-flex flex-column'>
                    <label>Select date range</label>
                    <DateRangePicker
                        isoWeek
                        format='yyyy-MM-dd'
                        onChange={(dateRangeArr) => getAsteroids(dateRangeArr)}
                        disabledDate={allowedMaxDays(7)}
                    />
                </Col>
                <AsteroidsData asteroidsData={data} />
            </Row>
        </Container>
    );
}

export default AsteroidsListing;