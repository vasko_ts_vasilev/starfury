import React, { useState } from 'react';
import { Col, Tabs, Tab } from 'react-bootstrap';

import AsteroidsTable from './table/AsteroidsTable';

const AsteroidsDataTable = ({ asteroidsData }) => {

    const [activeTab, setActiveTab] = useState();

    const nearEarthObjects = !asteroidsData.element_count ? null : asteroidsData.near_earth_objects;

    return (
        <Col sm='12' md='12' lg='12' className='pt-4'>
            <Tabs
                activeKey={activeTab}
                onSelect={setActiveTab}>
                {
                    nearEarthObjects && Object.entries(nearEarthObjects).map(entryArr => {

                        return <Tab
                            key={`${entryArr[0]}`}
                            eventKey={entryArr[0]}
                            title={entryArr[0]}>
                                <AsteroidsTable entryArr={entryArr} />
                        </Tab>
                    })
                }
            </Tabs>
        </Col>
    );
};

export default AsteroidsDataTable;