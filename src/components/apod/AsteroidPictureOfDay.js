import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { Container, Row, Col, Image } from 'react-bootstrap';

import loader from '../../resources/loader.gif';
import { getAsteroidPicOfTheDay } from '../../actions';

const AsteroidPictureOfDay = () => {

    const [today, _setToday] = useState(moment().format('YYYY-MM-DD'));
    const [data, setData] = useState({});

    useEffect(() => {

        async function fetchData() {
            const data = await getAsteroidPicOfTheDay({ date: today });

            setData(data);
        }

        fetchData();
    }, [today])

    return (
        <Container className=''>
            <h1 className='text-center pt-4'>Astronomy Picture Of The Day</h1>
            <Row className='pt-4 pb-4 justify-content-md-center'>
                <Col className='col-sm-12 col-md-10 col-lg-8 col-xl-8 text-center' >
                    <Image
                        src={!data?.url ? loader : data?.url}
                        style={{ border: '2px solid black' }}
                        alt={!data.title ? 'Astronomy Picture Of The Day' : data.title}
                        rounded
                        fluid
                    />
                    {data.title && <figcaption className='pt-4'>{data.explanation}</figcaption>}
                </Col>
            </Row>
        </Container>
    );
};

export default AsteroidPictureOfDay;