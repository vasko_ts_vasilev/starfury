import Axios from './axiosAPI';

import { getQueryParamsParams } from '../utils/commonUtils';

export const getAsteroidPicOfTheDay = async (params) => {

    const transformedParams = getQueryParamsParams(params);

    try {
        const response = await Axios.get(`/planetary/apod${transformedParams}`);

        return response.data;
    } catch (err) {
        console.log('SOMETHIN WENT WRONG ', err);
    }
};