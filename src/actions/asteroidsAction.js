import Axios from './axiosAPI';

import { getQueryParamsParams } from '../utils/commonUtils';

export const getAsteroidsData = async (params) => {

    const transformedParams = getQueryParamsParams(params);

    try {
        const response = await Axios.get(`/neo/rest/v1/feed${transformedParams}`);

        return response.data;
    } catch (err) {
        console.log('SOMETHIN WENT WRONG ', err);
    }
}